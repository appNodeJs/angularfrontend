import { Injectable } from '@angular/core';
import { WebService } from './web.service';
import { SERVER } from '../config/server.config';

@Injectable({
  providedIn: 'root'
})
export class AuthoritiesService {

  constructor(
    private webService: WebService
  ) { }

  getAll() {
    console.log(SERVER.AUTHORITIES);
    return this.webService.get(SERVER.AUTHORITIES);
  }
}
