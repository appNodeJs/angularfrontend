import { Injectable } from '@angular/core';
import { WebService } from './web.service';
import { SERVER } from '../config/server.config';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private webService: WebService
  ) { }

  getAll() {
    const URL = SERVER.USERS;
    console.log(URL);
    return this.webService.get(URL);
  }
}
