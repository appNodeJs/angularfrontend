import { Component, OnInit } from '@angular/core';
import { AuthoritiesService } from 'src/app/services/authorities.service';

@Component({
  selector: 'app-authorities',
  templateUrl: './authorities.component.html',
  styleUrls: ['./authorities.component.css']
})
export class AuthoritiesComponent implements OnInit {

  constructor(
    private authoritiesService: AuthoritiesService
  ) { }

  ngOnInit() {
    this.authoritiesService.getAll().subscribe(
      (res) => {
        console.log(res);
      }
    );
  }
}
