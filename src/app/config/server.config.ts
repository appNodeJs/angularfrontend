export const SERVER = (function() {
    const URL = {
        BASE: 'http://localhost:3000/api'
    };
    const AUTHORITIES = `authorities`;
    const USERS = `users`;
    return {
        URL_BASE: URL.BASE,
        AUTHORITIES: `${URL.BASE}/${AUTHORITIES}`,
        USERS: `${URL.BASE}/${USERS}`
    }
})();