import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthoritiesComponent } from './components/authorities/authorities.component';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './components/admin/navbar/navbar.component';
import { ArticleComponent } from './components/admin/article/article.component';
import { CommentComponent } from './components/admin/comment/comment.component';
import { UserComponent } from './components/admin/user/user.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthoritiesComponent,
    NavbarComponent,
    ArticleComponent,
    CommentComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
