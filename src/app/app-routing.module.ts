import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes }  from '@angular/router';
import { AuthoritiesComponent } from './components/authorities/authorities.component';
import { UserComponent } from './components/admin/user/user.component';

const routes: Routes = [
  { 
    path: 'authorities',
    component: AuthoritiesComponent 
  },
  {
    path: 'users',
    component: UserComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      routes, { useHash: true }
    )
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
